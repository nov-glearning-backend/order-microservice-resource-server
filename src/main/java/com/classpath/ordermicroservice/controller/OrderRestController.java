package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public Map<String, Object> fetchAllOrders(
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", required = false, defaultValue = "10") int size,
            @RequestParam(value = "sort", required = false, defaultValue = "asc") String direction,
            @RequestParam(value = "field", required = false, defaultValue = "customerName") String property){
        return this.orderService.fetchOrders(page, size, direction, property);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long id){
        return this.orderService.fetchOrderById(id);
    }


    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
       // order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrder(@PathVariable("id") long id, @RequestBody Order order){
        this.orderService.updateOrder(id, order);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long id){
        this.orderService.deleteOrderById(id);
    }
}

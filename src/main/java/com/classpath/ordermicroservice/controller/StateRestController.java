package com.classpath.ordermicroservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.availability.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/state")
@RequiredArgsConstructor
public class StateRestController {

    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher eventPublisher;

    @PostMapping("/liveness")
    public Map<String, Object> livenessState(){
        Map<String, Object> responseMap = new LinkedHashMap<>();
        LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
        LivenessState updatedLivenessState = currentLivenessState == LivenessState.CORRECT ? LivenessState.BROKEN : LivenessState.CORRECT;
        String state = updatedLivenessState == LivenessState.CORRECT ? "System Is UP" : "System is down";
        AvailabilityChangeEvent.publish(eventPublisher, state, updatedLivenessState);

        responseMap.put("liveness", updatedLivenessState);
        responseMap.put("state", state);
        return responseMap;
    }

    @PostMapping("/readiness")
    public Map<String, Object> readinessState(){
        Map<String, Object> responseMap = new LinkedHashMap<>();
        ReadinessState currentReadinessState = this.applicationAvailability.getReadinessState();
        ReadinessState updatedReadinessState = currentReadinessState == ReadinessState.ACCEPTING_TRAFFIC ? ReadinessState.REFUSING_TRAFFIC : ReadinessState.ACCEPTING_TRAFFIC;
        String state = updatedReadinessState == ReadinessState.ACCEPTING_TRAFFIC ? "System Is UP" : "System is down";
        AvailabilityChangeEvent.publish(eventPublisher, state, updatedReadinessState);

        responseMap.put("readiness", updatedReadinessState);
        responseMap.put("state", state);
        return responseMap;
    }
}

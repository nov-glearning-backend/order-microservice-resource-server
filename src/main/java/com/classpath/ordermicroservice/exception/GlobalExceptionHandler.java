package com.classpath.ordermicroservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleInvalidOrderId(IllegalArgumentException exception){
        return new Error(100, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Set<String>> handleInvalidOrder(MethodArgumentNotValidException exception){
        log.error("Exception while injesting the data :: {}", exception.getMessage());

        Map<String, Set<String>> errorResponse = new LinkedHashMap<>();
        var allErrors = exception.getAllErrors();
        Set<String> errorMessages = allErrors.stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        errorResponse.put("errors", errorMessages);
        return errorResponse;
    }

}
@Getter
@AllArgsConstructor
class Error {
    private int code;
    private String message;
}
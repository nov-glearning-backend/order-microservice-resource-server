package com.classpath.ordermicroservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public WebClient webClient(){
        WebClient webClient = WebClient.
                                    builder()
                                        .baseUrl("http://inventory-microservice-svc/")
                                        .build();
        return webClient;
    }
}

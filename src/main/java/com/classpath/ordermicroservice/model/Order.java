package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.weaver.Lint;

import javax.persistence.*;
import javax.sound.sampled.Line;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "customer name cannot be null")
    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;

    @Min(value = 1000, message = "min order price should be 1000")
    @Max(value = 50000, message = "max order price should be 50000")
    private double price;

    @PastOrPresent(message = "order time cannot be in future")
    private LocalDateTime time;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JsonManagedReference
    private Set<LineItem> lineItems;

    public void addLineItem(LineItem lineItem){
        if (this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}

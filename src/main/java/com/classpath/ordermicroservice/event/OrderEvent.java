package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.model.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
    private LocalDateTime orderTimeStamp;
}

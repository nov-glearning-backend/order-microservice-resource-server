package com.classpath.ordermicroservice.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@Slf4j
public class ApplicationStateChangeEventHandler {

    @EventListener
    public void livenessStateChangeHandler(AvailabilityChangeEvent<LivenessState> event){
      log.info("Liveness state changed :: ");
      log.info(event.getState().toString());
    }
    @EventListener
    public void readinessStateChangeHandler(AvailabilityChangeEvent<ReadinessState> event){
      log.info("Readiness state changed :: ");
      log.info(event.getState().toString());
    }
}
